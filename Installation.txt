Copy and paste these lines of code in the head tag:

<link rel="stylesheet" href="Sass/styles.css"> 
<script src="JavaScript/modal.js"></script>

How to override styling: 
Option 1(Easy): 
Open the styles.css file and write your code in there

Option 2(Hard): 
If you don't already have ruby installed in your computer you will need to download it from the 
ruby website. Then go to the Sass website and download Sass as well. Once you've downloaded these
 files open the terminal window in your IDE and navigate to the Sass folder. Then type in the 
terminal window "sass --watch sassStyling.scss:styles.css". Then you open the file called 
"sassStyling.scss" and write the code you want. After that, your styling code is applied to your 
website and you are good to go!